JavaScript - pro začátečníky (JVS1)
===================================

- **Lektor:** Ing. Jan kolomazník, Ph.D.
- **Mobil:** +420 732 568 669
- **Email:** jan.kolomaznik@gmail.com 

Účastníci získají základní přehled o jazyku JavaScript a jeho možnostech v prostředí internetových prohlížečů a nejen tam. 
Seznámí se se základní syntaxí tohoto jazyka, nejdůležitějšími funkcemi, vlastnostmi a získáte praktické návody pro reálné využití. 
Naučí se minimálně psát malé aplikace a vlastní knihovny do internetových stránek jako vysouvací menu, odesílat data z formulářů na pozadí nebo používat a zprovozňovat již hotové knihovny. 


- **[Úvod](/js)**
    * historie, hlavní přednosti a některé neduhy
    * knihovny
    * další možnosti využití – interpretry mimo prohlížeče
    * prohlížeče a nástroje
- **[Základ práce s daty](/js.value)**
    * primitivní datové typy
    * operátory – aritmetické, logické, porovnávací, řetězcové
    * základní funkce pro práci s proměnnými
- **[Programové struktury](/js.program)**
    * výrazy
    * operator přiřazení
    * podmínky if, else, switch, case
    * cykly for, while, do while
- **[Funkce](/js.function)**
    * co je funkce, způsoby vytváření funkcí
    * vracení výsledku, přerušení funkce
    * viditelnost proměnných (scope)
- **[Datové struktury](/js.data)**
    * základní objekty Array
- **[Uvod do Objektů](/js.object/)**
- *[Projekt _robot_](/js.projekt.robot)*
- **[Výjimky a chyby](/js.exception)**
    * objekt Console
    * ladění a krokování
    * chyby v syntaxi versus běhové chyby
    * zachytávání chyb pomocí try catch konstrukce
    * druhy vyvolaných chyb, instance typu Error
- **[Regulární výrazy](/js.regex)**
    * Definice refulárního výrazu
    * Jeho používání
    * Práce s řetězci
- **[Moduly](/js.module)**
    * Základy práce s npm
    * Import modulů
    * Vytváření modulů

