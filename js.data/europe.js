fetch("https://restcountries.com/v3.1/region/europe")
.then(response => response.json())
.then(data => {

    const europe = data.map(c => { return {
        name:c.name.common, 
        population:c.population }
    })
    const sum = europe.map(c => c.population)
                .reduce((a,b) => a + b );
    const avg = sum / europe.length
    console.log("Evropsy prumer = ", avg)

    console.log(europe.filter(c => c.population > avg))

    console.log(europe.reduce((a, b) => a.population < b.population ? a : b))


}).catch(err => console.error(err))
