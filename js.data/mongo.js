const { MongoClient, ServerApiVersion } = require('mongodb');
const uri = "mongodb+srv://tester_1:Yp6LRHZ1iXzr3KRa@ictpro.t5i1tq4.mongodb.net/?retryWrites=true&w=majority";

// Create a MongoClient with a MongoClientOptions object to set the Stable API version
const client = new MongoClient(uri, {
  serverApi: {
    version: ServerApiVersion.v1,
    strict: true,
    deprecationErrors: true,
  }
});

async function run() {
  try {
    // Connect the client to the server (optional starting in v4.7)
    await client.connect();
    const weresquirrel = client.db('weresquirrel')
    const jurnal = weresquirrel.collection('jurnal')

    /*
    console.log(
        await jurnal.find({ 'squirrel': true } )
                    .sort({ 'date': 1 })
                    .toArray()
    );
    */
    /*
    console.log(
        await jurnal.find( {
            'date': {
                $gte: new Date(2023,1,1),
                $lte: new Date(2023,1,10)
            } 
        }).sort( { date: 1 } )
          .toArray()
    );
    */
    const data = await jurnal.find({
        'date': {
            $gte: new Date(2023,1,1),
            $lte: new Date(2023,1,10)
        } 
    }).toArray()
    console.log(tableFor('pizza', data))

  } finally {
    // Ensures that the client will close when you finish/error
    await client.close();
  }
}

function phi(n00, n10, n01, n11) {
    return (n11*n00 - n10*n01) /
        Math.sqrt((n10 + n11) *
                  (n00 + n01) *
                  (n01 + n11) *
                  (n00 + n10))
}

console.log(phi(76, 9, 4, 1))

function tableFor(event, data) {
    let table = [0,0,0,0]
    data.forEach(day => {
        try {
            if (day['squirell'] && day['events'].includes(event)) {
                table[3] += 1
            } else if (day['squirell'] && !day['events'].includes(event)) {
                table[2] += 1
            } else if (!day['squirell'] && day['events'].includes(event)) {
                table[1] += 1
            } else if (!day['squirell'] && !day['events'].includes(event)) {
                table[0] += 1
            }
        } catch {
            console.warn(`Found day without events ${day._id}`)
        }
    });
    return table
}

run().catch(console.dir);