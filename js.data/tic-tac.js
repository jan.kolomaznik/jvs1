
function size(data) {
    return Math.floor(Math.sqrt(data.length))
}

function mrizka(data){
    const velikost = size(data)
    // Souradnice sloupcu (c)
    let line = ""
    for (let c = 0; c < velikost; c++) {
        line += `  ${c+1} ` 
    }
    console.log(line)
    // Radky (r)
    for (let r = 0; r < velikost; r++) {
        if (r !== 0){
            line = " "
            for (let c = 0; c < velikost; c++) {
                line += `${c?"+":""}---`
            }
            console.log(line)            
        }
        line = String.fromCharCode(65 + r)
        for (let c = 0; c < velikost; c++) {
            line += `${c?"|":""} ${data[(r*velikost)+c]} `
        }
        console.log(line)

    }
}

function put(data, coor, char) {
    const r = coor.charCodeAt(0) - 65
    const c = Number(coor[1]) - 1
    const velikost = size(data)

    const result = [...data]
    result[r*velikost + c] = char
    return result
}

data = ['O',' ',' ',' ','X',' ',' ',' ','O',]
data = put(data, 'B2', '!')
mrizka(data)