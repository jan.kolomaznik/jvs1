/*
let anObject = { left: 1, right: 2 };
console.log(anObject.left);
console.log(anObject['left']);

// → 1
delete anObject;
console.log(anObject.left);
// → undefined
console.log("left" in anObject);
// → false
console.log("right" in anObject);
// → true

console.log(Object.keys(anObject));

anObject.name = 'Honza'
anObject['Přijmeni'] = "Kolomaznik"
console.log(anObject)

for (item in anObject) {
    console.log(item);
}
*/

let menu = {
        "id": "file",
        "value": "File",
        "popup": {
            "menuitem": [
                { "value": "New", "onclick": "CreateNewDoc()" },
                { "value": "Open", "onclick": "OpenDoc()" },
                { "value": "Close", "onclick": "CloseDoc()" }
            ]
        }
    }
console.log(menu.popup.menuitem[0].onclick)

for (key in menu) {
    console.log(key, menu[key]);
}

menuitem = menu.popup.menuitem

console.log(menuitem.map(element => element.value));
console.log(menuitem.forEach(element => console.log(element.value)));
console.log(menuitem.filter(element => element.value.toLowerCase().includes('o')));
console.log(Math.max(...menuitem.map(element => element.value.length)));

let [prvni, druhy, a, b] = menuitem.map(element => element.value)
console.log(prvni, druhy, a, b);

let { id, ...value } = menu
console.log(id, value);

console.log(menuitem.map(element => element.value).join(' | '))

console.log(menuitem.map(element => element.value).every(e => e.startsWith('F')))

let sum = 0
menuitem.map(element => element.value.length).forEach(num => sum += num)


/*
async function fetchJson() {
    try {
        response = await fetch("https://restcountries.com/v3.1/region/europe")
        data = await response.json()
        console.log(data);
    } catch (err) {
        console.error(err)
    }
}
fetchJson()
*/
