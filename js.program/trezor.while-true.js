const prompt = require('prompt-sync')();

const secret = Math.floor(Math.random() * 10);

while (true) {

    const number = Number(prompt("Hadej cislo trezoru: "))
    if (Number.isInteger(number)) {
        if (number > secret) {
            console.log("Prilis velke")
        } else if (number < secret) {
            console.log("Moc male")
        } else {
            console.log("Uhadl jsi!!!")
            break // Preruseni cyklu
        }
    }
}
