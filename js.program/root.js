const prompt = require('prompt-sync')();

const theNumber = Number(prompt("Pick a number: "));

if (!Number.isNaN(theNumber) && theNumber > 0) {
    const root = Math.sqrt(theNumber);
    console.log(`Your number is ${theNumber} and the square root of ${root}`);
} else {
    console.log(`It's not number!!!`);
}
