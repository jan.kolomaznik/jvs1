const prompt = require('prompt-sync')();

const secret = Math.floor(Math.random() * 10);

while (true) {

    const number = Number(prompt("Hadej cislo trezoru: "))
    if (!Number.isInteger(number) || number < 0 || number > 9) {
        console.log("... cislici od 0 do 9!!")
        continue // Jdu na dalsi kolo
    }

    if (number > secret) {
        console.log("Prilis velke")
    } else if (number < secret) {
        console.log("Moc male")
    } else {
        console.log("Uhadl jsi!!!")
        break // Preruseni cyklu
    }
}
