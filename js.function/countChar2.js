function countChar(string, char = null, precate = null){
    let count = 0
    for (let i = 0; i < string.length; i++) {
        if (char && string[i] === char) {
            count++
        } else if (precate && precate(string[i])) {
            count++
        } 
        
    }
    return count
}

function countBs(string) {
    return countChar(string, char='B')
}

console.log(countBs("BBC"));
// → 2
console.log(
    countChar("kakKerlak", { precate:(c) => c.toLowerCase() === "k" }));
// → 4
