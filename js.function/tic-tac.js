
function mrizka(velikost = 3){
    // Souradnice sloupcu (c)
    let line = ""
    for (let c = 0; c < velikost; c++) {
        line += `  ${c+1} ` 
    }
    console.log(line)
    // Radky (r)
    for (let r = 0; r < velikost; r++) {
        if (r !== 0){
            line = " "
            for (let c = 0; c < velikost; c++) {
                line += `${c?"+":""}---`
            }
            console.log(line)            
        }
        line = String.fromCharCode(65 + r)
        for (let c = 0; c < velikost; c++) {
            line += `${c?"|":""}   `
        }
        console.log(line)

    }
}


mrizka()