function countChar(string, precate){
    let count = 0
    for (let i = 0; i < string.length; i++) {
        switch (typeof precate) {
            case 'string':
                if (string[i] === precate) {
                    count++
                } 
                break;
            case 'function':
                if (precate(string[i])) {
                    count++
                } 
                break;
        } 
        
    }
    return count
}

function countBs(string) {
    return countChar(string, 'B')
}

console.log(countBs("BBC"));
// → 2
console.log(countChar("kakKerlak", (c) => c.toLowerCase() === "k"));
// → 4
