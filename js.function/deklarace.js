'use strict'
let a, b, c

const PI = 3.1415
const kurz = "JVS1"

const square = function(a) {
    console.log("square", this)
    const result = a * a
    return result
}

function cube(a) {
    console.log("cube", this)
    return square(a) * a
}

const hyperCube = (a) => {
    console.log("hyperCube", this)
    return cube(a) * a;
}

console.log(PI, kurz, square)
console.log(a = square(5))
console.log(b = cube(5))
console.log(c = hyperCube(5))
console.log(a, b, c)
