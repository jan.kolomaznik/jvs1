// Vracim funkci z funkce, pmatuje si lokalni promenne
function sazba(procenta) {
    const vat = 1 + (procenta/100)
    return (caska) => caska * vat
}

const dan_21 = sazba(21)
const dan_17 = sazba(17)

console.log(dan_21(100))
console.log(dan_17(100))

// Pouziti funkce jako parameter jine funkce
function nakup(castka, dan = sazba(21)){
    console.log("Zplat", dan(castka));
}

nakup(100, sazba(15))
nakup(100)