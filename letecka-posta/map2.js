class Country {
    constructor(code, name, position, borders) {
        this.code = code;
        this.name = name;
        this.position = position;
        this.borders = borders;
    }
}

class Map {
    constructor(countries) {
        this.countires = countries
    }

    neibours(country) {
        const borders = country.borders
        return this.countires.filter(c => borders.includes(c.code))
    }
}

async function fetchMap(region) {
    try {
        const url = (region)
            ? `https://restcountries.com/v3.1/region/${region}`
            : `https://restcountries.com/v3.1/all`;

        let response = await fetch(url);
        let json = await response.json();
        let countires = json
            .map(c => new Country(c.cca3, c.name.common, c.latlng, c.borders))
            .filter(c => c.borders)
        return new Map(countires)
    } catch (err) {
        console.error(err)
    }
}

async function run() {
    const countries = await fetchMap('europe')
    console.log(countries)
}

exports.fetchMap = fetchMap




