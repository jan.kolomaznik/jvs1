class Country {
    constructor(code, name, position, borders) {
        this.code = code;
        this.name = name;
        this.position = position;
        this.borders = borders;
    }
}

async function fetchMap(region) {
    try {
        const url = (region)
            ? `https://restcountries.com/v3.1/region/${region}`
            : `https://restcountries.com/v3.1/all`;

        let response = await fetch(url);
        let json = await response.json();
        let countires = json
            .map(c => new Country(c.cca3, c.name.common, c.latlng, c.borders))
            .filter(c => c.borders)

        //const borders = [ 'AUT', 'CHE', 'CZE' ]
        //console.log(countires.filter(c => borders.includes(c.code)))

        countires.forEach(c => c.borders = countires.filter(cb => c.borders.includes(cb.code)));
        return countires
    } catch (err) {
        console.error(err)
    }
}

exports.fetchMap = fetchMap