let protoPes = {
    'name': null
}

protoPes.stekni = function() {
    return `${this.name}: HAF`
}

function createPes(name) {
    newPes = Object.create(protoPes)
    newPes.name = name
    return newPes
}

let hugo = createPes('Hugo')
hugo.stekni = function() {
    return `${this.name}: BARK`
}

let alfred = createPes('Alfred')

console.log(hugo.stekni());
console.log(alfred.stekni());
