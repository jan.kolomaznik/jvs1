function Kocka(name) {
    this.name = name
    this.isSleep = false
    this.sleep = function() {
        this.isSleep = !this.isSleep
        return this.isSleep
    }
}

Kocka.prototype.sound = function() {
    if (this.isSleep) {
        return `${this.name}: zzzzzz`
    } else {
        return `${this.name}: Mňouk`
    }
    
}

micka = new Kocka("Micka")

console.log(micka.sound())
console.log(micka.sleep())
console.log(micka.sound())


