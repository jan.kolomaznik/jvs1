// Zadani ax2 + bx + c = 0
a = 5
b = 1
c = -2

// Vypocet diskiminantu
D = Math.pow(b,2) + 4*a*c

// Vypocet korenu

x1 = (-b + Math.sqrt(D)) / (2*a)
x2 = (-b - Math.sqrt(D)) / (2*a)

// Vysledke
console.log(`Koreny jsou x1 = ${x1}, x2 = ${x2}.`)
